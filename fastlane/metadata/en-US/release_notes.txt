This update includes a number of improvements and bug fixes including:

- New optimised flow for adding works to your collection

Have feedback? Email us at support@artsy.net